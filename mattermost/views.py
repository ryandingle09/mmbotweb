#from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views import View
from .models import MattermostUser, MattermostPermission
import json

# class Api(View):

#     def get(self, request, mm_user_id):
#         user = MattermostUser.objects.filter(mattermost_user_id=mm_user_id)

#         if not user.exists():
#             return JsonResponse({'status_code': 402, 'message': 'Permission denied. User not found or not registered.'})

#         return JsonResponse({'status_code': 200, 'user_data': list(user.values()), 'user_role': list(user[0].role.all().values())})

class Api2(View):
    
    def get(self, request, mm_user_id):
        user = MattermostUser.objects.filter(mattermost_user_id=mm_user_id)

        if not user.exists():
            return JsonResponse({'status_code': 402, 'message': 'Permission denied. User not found or not registered.'})

        groups = user[0].group.all()
        provokes = user[0].revoke_permission.all()
        permission = []
        group_names = []
        grouping = {}

        for g in groups:
            perms = MattermostPermission.objects.filter(group=g.id)
            group_names.append(g.name.lower())
            pr = []
            
            for p in perms:
                permission.append(p.code)
                pr.append(p.code)
                grouping[g.name.lower()] = pr# if len(pr) != 0 else []

        if len(provokes) != 0:
            for pr in provokes:
                if pr.code in permission:
                    permission.remove(pr.code)

                for g in grouping:
                    if pr.code in grouping[g]:
                        grouping[g].remove(pr.code)

        return JsonResponse({'status_code': 200, 'user_data': list(user.values()), 'user_role': permission, 'user_group': group_names , 'grouping': grouping })



