from django.contrib import admin
from .models import MattermostUser, MattermostGroup, MattermostPermission

class MattermostUserAdmin(admin.ModelAdmin):
    list_display = ('mattermost_user_id', 'mattermost_name')
    list_filter = ('updated_at', 'added_at')
    search_fields = ['mattermost_user_id', 'mattermost_name']

# class MattermostRoleAdmin(admin.ModelAdmin):
#     list_display = ('role_code', 'role_name', 'role_description')
#     search_fields = ['role_code', 'role_name', 'role_description']

class MattermostGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    list_filter = ('name', 'added_at', 'updated_at')
    search_fields = ['name', 'description']

class MattermostPermissionAdmin(admin.ModelAdmin):
    list_display = ('code', 'description', 'group')
    list_filter = ('group', 'added_at', 'updated_at')
    search_fields = ['code', 'description', 'group__name']

admin.site.register(MattermostUser, MattermostUserAdmin)
admin.site.register(MattermostGroup, MattermostGroupAdmin)
admin.site.register(MattermostPermission, MattermostPermissionAdmin)
