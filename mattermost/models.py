from django.db import models
from django.utils import timezone

# class MattermostRole(models.Model):
#     id = models.AutoField(primary_key=True)
#     role_code = models.CharField(max_length=255, null=True)
#     role_name = models.CharField(max_length=255)
#     role_description = models.TextField(null=True)

#     def __str__(self):
#         return self.role_code

class MattermostUser(models.Model):
    id = models.AutoField(primary_key=True)
    mattermost_name = models.CharField(max_length=255)
    mattermost_user_id = models.CharField(max_length=255)
    # role = models.ManyToManyField(MattermostRole)
    group = models.ManyToManyField('MattermostGroup')
    revoke_permission = models.ManyToManyField('MattermostPermission', blank=True)
    added_at = models.DateField(auto_now_add=True) 
    updated_at = models.DateField(auto_now=True) 

    def __str__(self):
        return self.mattermost_user_id

class MattermostPermission(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    group = models.ForeignKey('MattermostGroup', on_delete=models.CASCADE, null=True)
    added_at = models.DateField(default=timezone.now) 
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.code


class MattermostGroup(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    added_at = models.DateField(default=timezone.now) 
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.name

