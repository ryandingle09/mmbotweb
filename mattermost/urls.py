from django.urls import path
from .views import Api2

urlpatterns = [
    # path('get_user_data/<str:mm_user_id>', Api.as_view()),
    path('v2/get_user_data/<str:mm_user_id>', Api2.as_view()),
]