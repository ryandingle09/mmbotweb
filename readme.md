# Use python3.7 to run this APP.

# This web application is built in python django 2.2.4 <https://www.djangoproject.com/>

Production Url: `http://devops-tools.southeastasia.cloudapp.azure.com/`
Admin: `http://devops-tools.southeastasia.cloudapp.azure.com/admin`

# Instructions

After cloning this source code from the git repository.
Make sure python3.7 is installed
Make sure you install mysql-server or mariadb mysql on your machine.

navigate to source project root directory.

Edit file `/projectrootfolder/vaderusermanagement/settings.py` and change the mysql database username, host, password and port to the appropriate database connection on your local machine.

1. Create a `virtualenv` specific for this app outside of it's project root directory.
 - `brew install virtualenv` or `pip install virtualenv` or `apt-get install virtualenv`
 - `virtualenv ../envfoldername` && `source ../envfoldername/bin/activate'

2. Create a database schema.
 - Run `./manage.py migrate`

3. Create a superuseradmin account.
 - RUN `./manage.py createsuperuser` # answer the console inputs.

4. Run the web application on the development server and explore it on the web browser (firefox, google chrome, etc..)
 - RUN `./manage.py runserver` open the browser and go to `http://localhost:8000/admin ` then login you superadmin account.
 - OR RUN if you want specific port `./manage.py runserver 0.0.0.0:9090` open the browser and go to `http://localhost:9090/admin ` then login you superadmin account.

5. Continue coding for the feature enhancement of this web app.
 - dont forget to create seperate branch for you features, fix, etc..
 - currently the updated branch for this is the `develop` branch.
 - after you push your updated code please don't forget to build and push the updated image in the azure container registry.
 - BUILD > `docker build -t ingasia.azurecr.io/containers/vaderusermanagement:latest .`
 - PUSH > `docker push ingasia.azurecr.io/containers/vaderusermanagement:latest`

6. If you have a release then you want to update the production app in `http://devops-tools.southeastasia.cloudapp.azure.com/`.
 - Open you mattermost account.
 - Find and Direct Message to vader using this command. `!exec service vader restart`
 - It will automatically update the production app in `http://devops-tools.southeastasia.cloudapp.azure.com/` into latest code in `develop` branch.

7. Happy Coding.