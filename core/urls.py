from django.urls import path
from .views import Index, Show

app_name = 'core'
urlpatterns = [
    path('', Index.as_view()),
    path('wiki/<int:id>', Show.as_view(), name='wiki'),
]