from django.db import models
from ckeditor.fields import RichTextField

class Link(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
  
    def __str__(self):
        return self.title

class CfGuildWiki(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    body = RichTextField(blank=True)
    added_at = models.DateField(auto_now_add=True) 
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.title
