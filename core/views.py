from django.views import View
from django.shortcuts import render
from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from .models import CfGuildWiki, Link

class Index(View):
    
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        # page        = int(request.GET['page']) if 'page' in request.GET else 1
        # search      = request.GET['search'] if 'search' in request.GET else False
        # items       = CfGuildWiki.objects.all().order_by('-id')
        # paginator   = Paginator(items, 10)
        # wikis       = paginator.get_page(page)

        # if search:
        #     items       = CfGuildWiki.objects.filter(title__icontains=str(search)).order_by('title')
        #     paginator   = Paginator(items, 10)
        #     wikis       = paginator.get_page(page)

        # data = {
        #     'items': wikis,
        #     'all_count': paginator.count
        # }
        

        allLinks = Link.objects.all()
        
        context = {
           'allLinks' : allLinks
	    }
 
        return render(request, self.template_name, context)

class Show(View):
    template_name = 'show.html'

    def get(self, request, id):
        item = get_object_or_404(CfGuildWiki, pk=id)

        data = {
            'item': item
        }

        return render(request, self.template_name, data)



