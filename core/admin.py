from django.contrib import admin

from .models import CfGuildWiki, Link

class CfGuildWikiAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ['title', 'body']

class LinkAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ['title']

admin.site.register(CfGuildWiki, CfGuildWikiAdmin)
admin.site.register(Link, LinkAdmin)
