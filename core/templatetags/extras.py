from django import template
from core.models import CfGuildWiki

def sidebarPost(type):
    return list(CfGuildWiki.objects.all().order_by('-id'))

def length(value):
    val = len(value)

    return val

register = template.Library()
register.filter('sidebar', sidebarPost)
register.filter('len', length)