from django.http import JsonResponse
from django.views import View
from .models import Log
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

import datetime

@method_decorator(csrf_exempt, name='dispatch')
class Index(View):
    
    def post(self, requests):
        action = requests.POST['action'] if 'action' in requests.POST else 'None'
        username = requests.POST['username'] if 'username' in requests.POST else 'None'

        activity_log = "%s performed >>> '%s' <<< on %s" % (username, action, datetime.datetime.now())

        perform = Log(activity=activity_log)
        perform.save()

        return JsonResponse({'status_code': 200, 'message': 'Log successfully saved.'})



