from django.urls import path
from .views import Index

app_name = 'activity'
urlpatterns = [
    path('log/save', Index.as_view()),
]