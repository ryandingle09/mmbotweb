from django.contrib import admin
from .models import Log

class ActivityLogAdmin(admin.ModelAdmin):
    list_display = ('activity', )
    search_fields = ['activity']

    # This will help you to disbale add functionality
    def has_add_permission(self, request):
        return False

    # This will help you to disable delete functionality
    #def has_delete_permission(self, request, obj=None):
    #    return False

    # This will help you to disable change functionality
    def has_change_permission(self, request, obj=None):
        return False

admin.site.register(Log, ActivityLogAdmin)
