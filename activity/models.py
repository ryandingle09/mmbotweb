from django.db import models

class Log(models.Model):
    id = models.AutoField(primary_key=True)
    activity = models.TextField()

    def __str__(self):
        return self.activity
